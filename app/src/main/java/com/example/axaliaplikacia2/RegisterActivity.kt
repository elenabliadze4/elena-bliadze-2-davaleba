package com.example.axaliaplikacia2

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegisterActivity : AppCompatActivity() {

    private lateinit var editTextEmailAddress : EditText
    private lateinit var editTextPassword : EditText
    private lateinit var editTextPassword2 : EditText
    private lateinit var buttonRegister : Button
    private lateinit var textView : TextView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        init()
        registrationListeners()

    }
    private fun init(){
        editTextEmailAddress = findViewById(R.id.editTextEmailAddress)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextPassword2 = findViewById(R.id.editTextPassword2)
        buttonRegister = findViewById(R.id.buttonRegister)
        textView = findViewById(R.id.textView)
    }

    private fun registrationListeners () {
        buttonRegister.setOnClickListener {
            val email = editTextEmailAddress.text.toString()
            val password = editTextPassword.text.toString()
            val repeatPassword = editTextPassword2.text.toString()

            if (email.isEmpty()|| password.isEmpty() || repeatPassword.isEmpty()) {
                Toast.makeText(this, "Please fill the blank space! ", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (password == repeatPassword) {
                FirebaseAuth
                    .getInstance()
                    .createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                                Toast.makeText(this, "Registration is successful", Toast.LENGTH_SHORT).show()
                                editTextEmailAddress.setText("")
                                editTextPassword.setText("")
                                editTextPassword2.setText("")
                                finish()
                            }else{
                                Toast.makeText(this, "You are registered already, so try sign in again, please", Toast.LENGTH_SHORT).show()
                                editTextEmailAddress.setText("")
                                editTextPassword.setText("")
                                editTextPassword2.setText("")

                            }

                    }
            }

        }
    }
}